﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class Game
    {

        #region fields and properties

        #region gameplay elements
        public SpaceShip playerShip = null;
        public Missile missile = null;
        public Bunker[] bunker = new Bunker[3];
        public SpaceShip ennemy = null;
        /// <summary>
        /// A dummy object just for demonstration
        /// </summary>
        #endregion

        #region game technical elements
        /// <summary>
        /// Size of the game area
        /// </summary>
        public Size gameSize;

        /// <summary>
        /// State of the keyboard
        /// </summary>
        public HashSet<Keys> keyPressed = new HashSet<Keys>();

        #endregion

        #region static fields (helpers)

        /// <summary>
        /// Singleton for easy access
        /// </summary>
        public static Game game { get; private set; }

        /// <summary>
        /// A shared black brush
        /// </summary>
        private static Brush blackBrush = new SolidBrush(Color.Black);

        /// <summary>
        /// A shared simple font
        /// </summary>
        private static Font defaultFont = new Font("Times New Roman", 24, FontStyle.Bold, GraphicsUnit.Pixel);
        #endregion

        #endregion

        #region constructors
        /// <summary>
        /// Singleton constructor
        /// </summary>
        /// <param name="gameSize">Size of the game area</param>
        /// 
        /// <returns></returns>
        public static Game CreateGame(Size gameSize)
        {
            if (game == null)
                game = new Game(gameSize);
            return game;
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        /// <param name="gameSize">Size of the game area</param>
        private Game(Size gameSize)
        {
            Bitmap imageShip = new Bitmap("../../Resources/ship4.png");
            playerShip = new SpaceShip(new Vecteur2D(gameSize.Width/2 - 30, gameSize.Height -50), 1, imageShip);
            ennemy = new SpaceShip(new Vecteur2D(gameSize.Width / 2 -30, gameSize.Height /9), 1, imageShip);
            for (int i=0;i<3;i++)
            {
                bunker[i] = new Bunker(new Vecteur2D((i+0)*(gameSize.Width/2.5), gameSize.Height - 150), 10);
            }
            this.gameSize = gameSize;
        }

        #endregion

        #region methods

        /// <summary>
        /// Draw the whole game
        /// </summary>
        /// <param name="g">Graphics to draw in</param>
        public void Draw(Graphics g)
        {
            playerShip.draw(g);
            if(ennemy != null)
            {
                 ennemy.draw(g);
            }
           
            for (int i = 0; i < 3; i++)
            {
                if(bunker[i] != null)
                {
                     bunker[i].Draw(g);
                }
               
            }
            if (missile != null)
            {
                missile.Draw(g);
            }
        }

        /// <summary>
        /// Update game
        /// </summary>
        public void Update(double deltaT)
        {  
            // keyboard events
            if (keyPressed.Contains(Keys.Space))
            {
                if(missile == null)
                {
                    missile = new Missile(new Vecteur2D(playerShip.position.x+30, playerShip.position.y), new Vecteur2D(0,-2), 10);
                }    
            }
            if(keyPressed.Contains(Keys.Left))
            {
                if(playerShip.position.x >0)
                {
                    playerShip.leftMove(deltaT);
                }
            }
            if(keyPressed.Contains(Keys.Right))
            {
                if(playerShip.position.x < gameSize.Width -65)
                {
                    playerShip.rigthMove(deltaT);
                }
            }
            if (missile != null)
            {
                if (missile.position.y > 0)
                {
                     missile.Move();
                    if(ennemy != null)
                    {
                        if(ennemy.Shoot(missile.position.x, missile.position.y))
                        {
                            ennemy = null;
                            missile = null;
                        }
                    }
                    for (int i=0; i<3;i++)
                    {
                        if(missile != null && bunker[i] !=null)
                        {
                            if (bunker[i].Colision(missile.position.x, missile.position.y))
                            {
                                missile = null;
                                bunker[i] = null;
                            }
                        }
                      
                    }
                }
                else
                {
                    missile = null;
                }
            }
            if(ennemy != null)
            {
                ennemy.leftMove(deltaT);
                if(ennemy.position.x == 0)
                {
                    ennemy.position.x = gameSize.Width - 65;
                }
            }
        }
        #endregion
    }
}
