﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class SpaceShip
    {
        public Vecteur2D position;
        public int lives;
        public bool alive { get; }
        public Bitmap image;

        public SpaceShip(Vecteur2D position, int lives, Bitmap image)
        {
            this.position = position;
            this.lives = lives;
            this.image = image;
        }

        public void draw(Graphics g)
        {
            g.DrawImage(image, new Point((int)position.x,(int)position.y));
        }

        public void leftMove(double deltaT)
        {
            position.x -= 1;
        }

        public void rigthMove(double deltaT)
        {
            position.x += 1;
        }
        public bool Shoot(double x, double y)
        {
            if ((this.position.x < x) && (x < (this.position.x + this.image.Width)) && (this.position.y < y) && (y < (this.position.y + this.image.Height)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
