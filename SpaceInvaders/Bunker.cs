﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Bunker
    {
        public Vecteur2D position;
        public Bitmap image;
        public int lives;
        public bool alive;

        public Bunker(Vecteur2D position, int lives)
        {
            this.position = position;
            this.lives = lives;
            this.image = new Bitmap("../../Resources/bunker.png");
        }

        public void Draw(Graphics g)
        {
            g.DrawImage(image, new Point((int)position.x, (int)position.y));
        }

        public bool Colision(double x, double y)
        {
            if ((this.position.x < x)&& (x < (this.position.x + this.image.Width)) && (this.position.y < y) && (y < (this.position.y + this.image.Height)))
            {
                Console.WriteLine("toucher");
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
