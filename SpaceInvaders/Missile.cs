﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Missile
    {
        public Vecteur2D position;
        public Vecteur2D vitesse;
        public int lives;
        public bool alive { get; }
        public Bitmap image;

        public Missile(Vecteur2D position, Vecteur2D vitesse, int lives)
        {
            this.position = position;
            this.vitesse = vitesse;
            this.lives = lives;
            this.image = new Bitmap("../../Resources/shoot1.png");
        }

        public void Draw(Graphics g)
        {
            g.DrawImage(image, new Point((int) position.x, (int)position.y));
        }

        public void Move()
        {
            this.position = this.position.addition(this.vitesse);
        }
    }
}
