﻿using System;
using NUnit.Framework;

//Pensez à ajouter le package NUNIT

namespace SpaceInvaders
{
	[TestFixture]
	public class TestVecteur2D
	{
		[SetUp]
		public void beforeTest(){

		}
		[Test]
		public void Test1()
		{
			Vecteur2D v1 = new Vecteur2D ();
			Assert.AreEqual (v1.Norme, 0);

		}
		[Test]
		public void Test2()
		{
			Vecteur2D v1 = new Vecteur2D (1,1);
			Vecteur2D v2 = new Vecteur2D (2,2);

			Assert.AreEqual (v1.Norme, Math.Sqrt(2));
			Assert.AreEqual (v2.Norme, Math.Sqrt(8));


		}
		[Test]
		public void Test3()
		{
			Vecteur2D v1 = new Vecteur2D (1,1);
			Vecteur2D v2 = new Vecteur2D (2,2);
			Vecteur2D v3 = v1.addition (v2);

			Assert.AreEqual (v3.Norme, Math.Sqrt(18));



		}
		[Test]
		public void Test4()
		{
			Vecteur2D v1 = new Vecteur2D (2,2);
			Vecteur2D v2 = new Vecteur2D (5,5);
			Vecteur2D v3 = v1.soustraction (v2);

			Assert.AreEqual (v3.Norme, Math.Sqrt(18));



		}
		[Test]
		public void Test5()
		{
			Vecteur2D v1 = new Vecteur2D (2,2);

			Vecteur2D v2 = v1.unaire ();
			Assert.AreEqual (v1.addition(v2).Norme, 0);



		}
		[Test]
		public void Test6()
		{
			Vecteur2D v1 = new Vecteur2D (2,2);

			Vecteur2D v2 = v1.multiplication (2);
			Assert.AreEqual (v2.Norme, Math.Sqrt(32));



		}
		[Test]
		public void Test7()
		{
			Vecteur2D v1 = new Vecteur2D (4,6);

			Vecteur2D v2 = v1.division (2);
			Assert.AreEqual (v2.Norme, Math.Sqrt(13));



		}
	}
}

