﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Vecteur2D
    {
        public double x, y;

        public Vecteur2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double Norme
        {
             get
            {
                return this.Norme;
            }  
        }

        public Vecteur2D addition(Vecteur2D v)
        {
            return new Vecteur2D(this.x + v.x, this.y + v.y);
        }

        public Vecteur2D soustraction(Vecteur2D v)
        {
            return new Vecteur2D(this.x - v.x, this.y - v.y);
        }

        public Vecteur2D moinsUnaire(Vecteur2D v)
        {
            return new Vecteur2D(-this.x - v.x, -this.y - v.y);
        }

        public Vecteur2D multiplication(Vecteur2D v)
        {
            return new Vecteur2D(this.x * v.x, this.y * v.y);
        }
    }
}

